class AddValues {
  int? modelServiceId, servicePrice, modelCoworkerId, serviceDuration;
  String? serviceName, serviceRate, serviceDescription;
  AddValues(
      {this.modelServiceId,
      this.servicePrice,
      this.modelCoworkerId,
      this.serviceDuration,
      this.serviceName,
      this.serviceRate,
      this.serviceDescription});
}
