class Users {
  int? id, isVerified, status;
  String? name,
      image,
      phone,
      phoneCode,
      otp,
      deviceToken,
      email,
      provider,
      providerToken,
      emailVerifiedAt,
      createdAt,
      updatedAt,
      token,
      imagePath,
      completeImage;

  Users(
      {this.id,
      this.name,
      this.image,
      this.phone,
      this.phoneCode,
      this.isVerified,
      this.otp,
      this.deviceToken,
      this.email,
      this.status,
      this.provider,
      this.providerToken,
      this.emailVerifiedAt,
      this.createdAt,
      this.updatedAt,
      this.token,
      this.imagePath,
      this.completeImage});
  factory Users.fromJson(Map<String, dynamic> json) {
    return Users(
        id: json['id'],
        name: json['name'],
        image: json['image'],
        phone: json['phone'],
        phoneCode: json['phone_code'],
        isVerified: json['is_verified'],
        otp: json['otp'],
        deviceToken: json['device_token'],
        email: json['email'],
        status: json['status'],
        provider: json['provider'],
        providerToken: json['provider_token'],
        emailVerifiedAt: json['email_verified_at'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        token: json['token'],
        imagePath: json['imagePath'],
        completeImage: json['completeImage']);
  }
}
